var express = require("express");
var fs = require("fs");
var moment = require("moment");

var app = express();
var cors = require('cors');
app.use(cors({origin:true, credentials:true}));

var storessid = "";
var storepassword = "";
var storewebkey = "";
var storerange = "";
var storebuzzer = "";
var storeui1 = "";
var storeui2 = "";
var storeui3 = "";
var storeui4 = "";
var storeui5 = "";
var storeuo1 = "";
var storeuo2 = "";
var storeuo3 = "";
var storeuo4 = "";
var storeuo5 = "";
var storeactivedooropen = "";
var storeactivedoorclose = "";
var storetimeon = "";
var storetimeoff = "";
var storerotate = "";
var triggeropen = "false";
var triggerclosed = "false";

app.get("/", (request, response) => {
  response.sendStatus(200);
});

app.get("/ssid", (request, response) => {
  response.send(storessid);
});

app.get("/password", (request, response) => {
  response.send(storepassword);
});

app.get("/webkey", (request, response) => {
  response.send(storewebkey);
});

app.get("/range", (request, response) => {
  response.send(storerange);
});

app.get("/buzzer", (request, response) => {
  response.send(storebuzzer);
});

app.get("/ui1", (request, response) => {
  response.send(storeui1);
});

app.get("/ui2", (request, response) => {
  response.send(storeui2);
});

app.get("/ui3", (request, response) => {
  response.send(storeui3);
});

app.get("/ui4", (request, response) => {
  response.send(storeui4);
});

app.get("/ui5", (request, response) => {
  response.send(storeui5);
});

app.get("/uo1", (request, response) => {
  response.send(storeuo1);
});

app.get("/uo2", (request, response) => {
  response.send(storeuo2);
});

app.get("/uo3", (request, response) => {
  response.send(storeuo3);
});

app.get("/uo4", (request, response) => {
  response.send(storeuo4);
});

app.get("/uo5", (request, response) => {
  response.send(storeuo5);
});

app.get("/activedooropen", (request, response) => {
  response.send(storeactivedooropen);
});

app.get("/activedoorclose", (request, response) => {
  response.send(storeactivedoorclose);
});

app.get("/triggerdooropen", (request, response) => {
  triggeropen = "true";
  response.send(triggeropen);
});

app.get("/triggerdoorclosed", (request, response) => {
  triggerclosed = "true";
  response.send(triggerclosed);
});

app.get("/disabletriggerdooropen", (request, response) => {
  triggeropen = "false";
  response.send(triggeropen);
});

app.get("/disabletriggerdoorclosed", (request, response) => {
  triggerclosed = "false";
  response.send(triggerclosed);
});

app.get("/getdooropen", (request, response) => {
  response.send(triggeropen);
});

app.get("/getdoorclosed", (request, response) => {
  response.send(triggerclosed);
});

app.get("/rotate", (request, response) => {
  response.send(storerotate);
});

app.get("/timeon", (request, response) => {
  response.send(storetimeon);
});

app.get("/timeoff", (request, response) => {
  response.send(storetimeoff);
});

app.get("/realTime", (request, response) => {
  var realTime = moment().format("HH:mm");
  response.send(realTime);
});

app.get("/setzen/:ssid/:password/:webkey/:range/:buzzer/:ui1/:ui2/:ui3/:ui4/:ui5/:uo1/:uo2/:uo3/:uo4/:uo5/:dooropen/:doorclose/:timeon/:timeoff", (request, response) => {
  var { ssid, password, webkey, range,  buzzer, ui1, ui2, ui3, ui4, ui5, uo1, uo2, uo3, uo4, uo5, dooropen, doorclose, timeon, timeoff } = request.params;
  storessid = ssid;
  storepassword = password;
  storewebkey = webkey;
  storerange = range;
  storebuzzer = buzzer;
  storeui1 = ui1;
  storeui2 = ui2;
  storeui3 = ui3;
  storeui4 = ui4;
  storeui5 = ui5;
  storeuo1 = uo1;
  storeuo2 = uo2;
  storeuo3 = uo3;
  storeuo4 = uo4;
  storeuo5 = uo5;
  storeactivedooropen = dooropen;
  storeactivedoorclose = doorclose;
  storetimeon = timeon;
  storetimeoff = timeoff;
  response.send(ssid, password, webkey, range, buzzer, ui1, ui2, ui3, ui4, ui5, uo1, uo2, uo3, uo4, uo5, dooropen, doorclose, timeon, timeoff);
});

app.get("/setardor/:ssid/:password/:rotate", (request, response) => {
  var{ssid, password, rotate} = request.params;
  storessid = ssid;
  storepassword = password;
  storerotate = rotate;
  response.send(ssid, password, rotate);
});

app.listen(3000, () => console.log("App Listening on port 3000"));

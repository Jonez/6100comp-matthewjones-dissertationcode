void setup() {
  Serial.begin(115200);
  wifiSet();
  getRotate();
  getWifiData();
  pinMode(motor1Pin1, OUTPUT);
  pinMode(motor1Pin2, OUTPUT);
  pinMode(enable1Pin, OUTPUT);
  ledcSetup(pwmChannel, freq, resolution);
  ledcAttachPin(enable1Pin, pwmChannel);
  linear.attach(linearPin, 500, 2300); 
}

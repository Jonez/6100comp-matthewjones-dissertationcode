void getRotate(){
  HTTPClient http;
  int httpCode;
  
  http.begin(IP + "/rotate");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload1 = http.getString();
        rotate = payload1.toInt();
      }
    }
   http.end();
}

void getWifiData(){
//  HTTPClient http;
//  int httpCode;
  //  http.begin(IP + "/ssid");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload1 = http.getString();
//        ssid = payload1;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/password");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload2 = http.getString();
//        password = payload2;
//      }
//    }
//    http.end();
}

void triggerData(){
  HTTPClient http;
  int httpCode;
  
  http.begin(IP + "/getdooropen");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload1 = http.getString();
        doorOpen = payload1;
      }
    }
   http.end();

   http.begin(IP + "/getdoorclosed");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload2 = http.getString();
        doorClose = payload2;
      }
    }
   http.end();
}

void triggerFalse(){
  if(doorOpen == "true" || doorClose == "true"){
    HTTPClient http;
    int httpCode;
  
    http.begin(IP + "/disabletriggerdooropen");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload1 = http.getString();
        doorOpen = payload1;
      }
    }
    http.end();

    http.begin(IP + "/disabletriggerdoorclosed");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload2 = http.getString();
        doorClose = payload2;
      }
    }
   http.end();
  }
}

/*
 * Creator Matthew Jones 
 * Product Zen
 * Company Ardmo
 * DateStarted 14/04/2019
 * DateFinished 
 */

#include <Wire.h>
extern "C" { 
#include "utility/twi.h"  // from Wire library, so we can do bus scanning
}

#include "Adafruit_VL53L0X.h"
Adafruit_VL53L0X lox = Adafruit_VL53L0X();

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include <HTTPClient.h>

#define TCAADDR 0x70

#define PIN 26
#define NUMPIXELS 12
#define DELAYVAL 100

Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define buzzer 27
#define button 15

String webEvent;
String webKey = "banOzFc5SnXHWbbNQ5wXPC";
String IP = "http://192.168.43.230:3000";

String timeOn;
String timeOff;

int ledNum = 2;
int pCount = 0;
int rangeDistance = 600;
int tick = 0;
int tempCheck = 0;
unsigned long previousMillis = 0; // will store last time LED was updated
const long interval = 1000; // constants won't change

bool selectedMode = false;
bool pastSelected = false;
bool sensorIn = false;
bool sensorOut = false;
bool motion = false;
String buzzerOn = "false";
String activedooropen = "no";
String activedoorclose = "no";
bool falseBuzz = false;
bool sleepMode = false;

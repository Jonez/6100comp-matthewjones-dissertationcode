void neoPixelSet() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  pixels.begin();
}

void neoPixel() {
  registerBuzzer();
  for (int i = 0; i <= NUMPIXELS; i++) {
    neoColorMode(i,0,255,0);
    delay(DELAYVAL);
    neoColorMode(i,0,0,0);
  }
}

void neoColorMode(int ledNum, int R, int G, int B) {
  pixels.clear();
  pixels.setPixelColor(ledNum, pixels.Color(R, G, B));
  delay(1);
  pixels.show();
}

void neoClear(int delayTime) {
  delay(delayTime);
  pixels.clear();
}

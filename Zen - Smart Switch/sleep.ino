String realTime;
void checkTime(){
  HTTPClient http;
  int httpCode;
  
    http.begin(IP + "/realTime");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload1 = http.getString();
        realTime = payload1;
      }
    }
    http.end();

    if(timeOn == realTime){
      sleepMode = false;
    }
    if(timeOff == realTime){
      sleepMode = true;
    }
}

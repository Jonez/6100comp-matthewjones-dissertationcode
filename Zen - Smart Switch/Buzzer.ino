
int freq = 2000;
int channel = 0;
int resolution = 10;

void buzzerSet(){
  pinMode(buzzer, OUTPUT);
  ledcSetup(channel, freq, resolution);
  ledcAttachPin(buzzer, channel);
  
}

void registerBuzzer(){
  if(buzzerOn == "true"){
      ledcWrite(channel, 125);
      delay(150);
      ledcWrite(channel, 0);
  }
}

void getData(){ //requires a restart of the esp
  HTTPClient http;
  int httpCode;
  
//  http.begin(IP + "/ssid");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload1 = http.getString();
//        ssid = payload1;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/password");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload2 = http.getString();
//        password = payload2;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/webkey");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload3 = http.getString();
//        webkey = payload3;
//      }
//    }
//    http.end();

    http.begin(IP + "/range");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload4 = http.getString();
        rangeDistance = payload4.toInt();
      }
    }
    http.end();
    
    http.begin(IP + "/buzzer");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload5 = http.getString();
        buzzerOn = payload5;
      }
    }
    http.end();
    
//    http.begin(IP + "/ui1");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload6 = http.getString();
//        zenIn1 = payload6;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/ui2");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload7 = http.getString();
//        zenIn2 = payload7;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/ui3");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload8 = http.getString();
//        zenIn3 = payload8;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/ui4");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload9 = http.getString();
//        zenIn4 = payload9;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/ui5");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload10 = http.getString();
//        zenIn5 = payload10;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/uo1");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload11 = http.getString();
//        zenOut1 = payload11;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/uo2");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload12 = http.getString();
//        zenOut2 = payload12;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/uo3");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload13 = http.getString();
//        zenOut3 = payload13;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/uo4");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload14 = http.getString();
//        zenOut4 = payload14;
//      }
//    }
//    http.end();
//    
//    http.begin(IP + "/uo5");
//    httpCode = http.GET();
//    if(httpCode > 0) {
//      if(httpCode == HTTP_CODE_OK) {
//        String payload15 = http.getString();
//        zenOut5 = payload15;
//      }
//    }
//    http.end();

    http.begin(IP + "/timeon");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload16 = http.getString();
        timeOn = payload16;
      }
    }
    http.end();

    http.begin(IP + "/timeoff");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload17 = http.getString();
        timeOff = payload17;
      }
    }
    http.end();

    http.begin(IP + "/activedooropen");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload18 = http.getString();
        activedooropen = payload18;
      }
    }
    http.end();

    http.begin(IP + "/activedoorclose");
    httpCode = http.GET();
    if(httpCode > 0) {
      if(httpCode == HTTP_CODE_OK) {
        String payload19 = http.getString();
        activedoorclose = payload19;
      }
    }
    http.end();

    Serial.println("Got ALL Data");
}

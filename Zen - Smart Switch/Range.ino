
void rangeSet() {

  Wire.begin();
  
  tcaselect(0);
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while (1);
  }

  tcaselect(1);
  if (!lox.begin()) {
    Serial.println(F("Failed to boot VL53L0X"));
    while (1);
  }
}

void rangeRead() {
    VL53L0X_RangingMeasurementData_t measure;
    tcaselect(0);
    lox.rangingTest(&measure, false); // pass in 'true' to get debug data printout!
    
    int milimeter = measure.RangeMilliMeter;
    unsigned long currentMillis = millis();
    falseBuzz = false;
   
    if(milimeter <= rangeDistance){   
      if (currentMillis - previousMillis >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;
        if(measure.RangeMilliMeter <= rangeDistance){
          tick++;
        }      
      }  
      pastSelected = false;
          // will run when tick equals 5 seconds
          if (tick >= 5) {
            motion = false;
            selectedMode = true;
            if (milimeter >= 1 && milimeter < 100) {
              ledNum = 2;
            }
            if (milimeter >= 100 && milimeter < 150) {
              ledNum = 4;
            }
            if (milimeter >= 150 && milimeter < 200) {
              ledNum = 6;
            }
            if (milimeter >= 200 && milimeter < 250) {
              ledNum = 8;
            }
            if (milimeter >= 250 && milimeter <= 300) {
              ledNum = 10;
            }
            if(milimeter > 300){
              ledNum = 20;
              falseBuzz = true;
              neoClear(1);
            }
            pixels.clear();
            delay(40);
            modeLED(ledNum);
            countFiveTicks();
        }
        modeLEDSelected(ledNum);
    } 
    else {
          tick = 0;
          tempCheck = 0;
          selectedMode = false;
    }

    unsigned long motionMillis = 0;
    
    tcaselect(0);
    lox.rangingTest(&measure, false); 
    
    if(tick < 5){
      if(measure.RangeMilliMeter <= rangeDistance){
        motion = true;
        sensorIn = true;
      }
      if(pastSelected == false){
        if(motion == true && selectedMode == false && sensorIn == true){
          if(currentMillis - motionMillis >= interval){
            if(tick == 0 && milimeter > rangeDistance){
              pCount++;
              Serial.println(pCount);
              if(pCount == 1){
                activeSensorIn(ledNum);
                neoPixel();
              }
              if(pCount > 1){
                delay(2000);
              }
              sensorIn = false;
              motion = false;
            }
          }
          motionMillis = currentMillis;
        }
      }
    }

    tcaselect(1);
    lox.rangingTest(&measure, false);
     
    if(tick < 5){
      if(measure.RangeMilliMeter <= rangeDistance){
        motion = true;
        sensorOut = true;
      }
      if(pastSelected == false){
        if(motion == true && selectedMode == false && sensorOut == true){
          if(currentMillis - motionMillis >= interval){
            if(tick == 0 && milimeter > rangeDistance){
              if(pCount > 0){
                pCount--;
              }
              Serial.println(pCount);
              if(pCount == 0){
                activeSensorOut(ledNum+1);
                neoPixel();
              }
              if(pCount > 0){
                delay(2000);
              }
              sensorOut == false;
              motion = false;
            }
          }
          motionMillis = currentMillis;
        }
      }
    }
}

void countFiveTicks(){
  if(tick == 5){
    tempCheck = 10;
  }
}

void modeLEDSelected(int ledNum){
  unsigned long selectedMillis = 0;
  //Counts Ticks To Change Mode 
  if(tick == 10){
    neoColorMode(ledNum, 0, 0, 255);
    registerBuzzer();
    motion = false;
    selectedMode = true;
    delay(1000);
    pastSelected = true;
    neoColorMode(ledNum, 0, 0, 0);
    tick = 0;
  }
}

void modeLED(int ledNum){
  neoColorMode(ledNum, 0, 255, 0);
}

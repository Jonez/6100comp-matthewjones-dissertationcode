import 'expo/build/Expo.fx';
import React from 'react';
import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";
import ZenScreen from "./src/Screens/Zen";
import ArdorScreen from "./src/Screens/Ardor";
import Header from './src/Screens/Shared/header.js'

const AppNavigator = createStackNavigator(
  {
    ZenScreen: {
      screen: ZenScreen,
      title: "Zen",
    },
    ArdorScreen: {
      screen: ArdorScreen,
      title: "Ardor",
    }
  },
  {
    defaultNavigationOptions: {
      headerTitle:()=> <Header/>,
      headerLeft: null,
      headerStyle: {
        backgroundColor: "#7e7e7e",
        height:80
      }
    }
  }
);

export default createAppContainer(AppNavigator);

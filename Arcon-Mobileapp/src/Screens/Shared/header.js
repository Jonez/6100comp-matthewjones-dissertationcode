import React from "react";
import { StyleSheet, Image, Text, TouchableOpacity, View, ScrollView } from "react-native";
import logo from '../../../assets/ardmo.png';

export default function Header(){
    return(
      <View style={styles.container}>
        <Image source={(logo)} style={styles.logo}/>
      </View>
    )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#e7e7e7',
    alignItems: 'center',
    position:'relative',
    top:0,
    padding:0
  },
  logo:{
    width:110,
    height:110,
    position:'absolute',
    zIndex:999,
    marginTop:-40,
    borderColor:'#7e7e7e',
    borderWidth:10,
    borderRadius:100
  },
});
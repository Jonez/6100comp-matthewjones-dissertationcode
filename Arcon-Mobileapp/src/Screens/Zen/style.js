import { ScrollView, View, Text, TouchableOpacity } from "react-native";
import styled from "styled-components";

export const Container = styled(View)`
  flex: 1;
  padding: 10% 0 0 0;
  align-items: center;
  background-color: #e7e7e7;
`;

export const Card = styled(View)`
  background-color: white;
  justify-content: ${props => props.justifyContent || "flex-start"};
  width: ${props => props.width || "90%"};
  height: ${props => props.height || "auto"};
  marginTop:10;
  borderRadius: 4;
  shadowColor: "#000",
  shadowOffset: {
	    width: 0;
	    height: 2;
   }
  shadowOpacity: 0.3;
  shadowRadius: 3.84;
  elevation: 5;
  marginLeft:20px;
`;

export const LastCard = styled(View)`
  background-color: white;
  justify-content: ${props => props.justifyContent || "flex-start"};
  width: ${props => props.width || "90%"};
  height: ${props => props.height || "auto"};
  marginTop:10;
  borderRadius: 4;
  shadowColor: "#000",
  shadowOffset: {
	    width: 0;
	    height: 2;
   }
  shadowOpacity: 0.3;
  shadowRadius: 3.84;
  elevation: 5;
  marginLeft:20px;
  marginBottom:80px;
`;

export const Direction = styled(View)`
  flexDirection: row;
  position:absolute;
`;

export const Nav = styled(TouchableOpacity)`
  width:50%;
  height:40;
  backgroundColor:#d4d4d4;
`;

export const NavText = styled(Text)`
  textAlign:center;
  fontSize:30;
  color:#7e7e7e;
`;

export const CustomScrollView = styled(ScrollView)`
  width: ${props => props.width || "100%"};
`;

export const Save = styled(TouchableOpacity)`
  backgroundColor:#009b00;
  width:100%;
  height:50;
  position:absolute;
  bottom:0;
`;

export const SaveText = styled(Text)`
  color: white;
  textAlign:center;
  fontSize:30;
`;

export const CardTitle = styled(Text)`
  fontSize:25;
  paddingLeft:15;
  paddingTop:5;
  color:#7e7e7e;
`;
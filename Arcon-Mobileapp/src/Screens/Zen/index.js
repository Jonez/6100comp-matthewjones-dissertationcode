import React, { Component } from "react";
import { Text, TouchableOpacity, View, ScrollView, Switch } from "react-native";
import { CustomInput } from "../../Components/CustomInput";
import instance from "../../Config/Axios";
import { Card, Container, Direction, Nav, NavText, CustomScrollView, Save, SaveText, CardTitle, LastCard} from "./style";


class ZenScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tssid: "",
      tpassword: "",
      twebkey: "",
      trange: "",
      tbuzzer: "",
      tui1: "",
      tui2: "",
      tui3: "",
      tui4: "",
      tui5: "",
      tuo1: "",
      tuo2: "",
      tuo3: "",
      tuo4: "",
      tuo5: "",
      tdooropen: "",
      tdoorclose: "",
      ttimeon: "",
      ttimeoff: ""
    };
  }

  submitAnswer = () => {
    var ssid = this.state.tssid;
    var password = this.state.tpassword;
    var webkey = this.state.twebkey;
    var range = this.state.trange;
    var buzzer = this.state.tbuzzer;
    var ui1 = this.state.tui1;
    var ui2 = this.state.tui2;
    var ui3 = this.state.tui3;
    var ui4 = this.state.tui4;
    var ui5 = this.state.tui5;
    var uo1 = this.state.tuo1;
    var uo2 = this.state.tuo2;
    var uo3 = this.state.tuo3;
    var uo4 = this.state.tuo4;
    var uo5 = this.state.tuo5;
    var dooropen = this.state.tdooropen;
    var doorclose = this.state.tdoorclose;
    var timeon = this.state.ttimeon;
    var timeoff = this.state.ttimeoff;
    var send = "/setzen/" + String(ssid) + "/" + String(password) + "/" +  String(webkey) + "/"  + String(range) + "/" + String(buzzer) + "/" + String(ui1) + "/" + String(ui2) + "/" + String(ui3) + "/" + String(ui4) + "/" + String(ui5) + "/" + String(uo1) + "/" + String(uo2) + "/" + String(uo3) + "/" + String(uo4) + "/" + String(uo5) + "/" + String(dooropen) + "/" + String(doorclose) + "/" + String(timeon) + "/" + String(timeoff); 
    console.log(send);
    instance
      .get(send)
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
        <Container>
          <Direction>
            <Nav onPress={() => this.props.navigation.navigate('ZenScreen')}>
              <NavText>Zen</NavText>
            </Nav>
            <Nav onPress={() => this.props.navigation.navigate('ArdorScreen')}>
              <NavText>Ardor</NavText>
            </Nav>
          </Direction>

          <CustomScrollView showsVerticalScrollIndicator={false}>
          
          <Card alignItems={"flex-start"}>
            <CardTitle>WIFI</CardTitle>
            <CustomInput label="ssid" width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tssid: text
              })
            }></CustomInput>
            <CustomInput label="password" width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tpassword: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>IFTTT Webkey</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                twebkey: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>Sensor Range (mm)</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                trange: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>Buzzer</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tbuzzer: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User In 1</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tui1: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User In 2</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tui2: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User In 3</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tui3: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User In 4</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tui4: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User In 5</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tui5: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User Out 1</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tuo1: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User Out 2</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tuo2: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User Out 3</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tuo3: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User Out 4</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tuo4: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>User Out 5</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tuo5: text
              })
            }></CustomInput>
          </Card>
          
          <Card alignItems={"flex-start"}>
            <CardTitle>Trigger door open?</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tdooropen: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>Trigger door close?</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tdoorclose: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>Time to go off</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                ttimeoff: text
              })
            }></CustomInput>
          </Card>

          <LastCard alignItems={"flex-start"}>
            <CardTitle>Time to come on</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                ttimeon: text
              })
            }></CustomInput>
          </LastCard>
          </CustomScrollView>

          <Save onPress={() => this.submitAnswer()}>
            <SaveText>Save</SaveText>
          </Save>

        </Container>
    );
  }
}

export default ZenScreen;

import React, { Component } from "react";
import { Dimensions, ScrollView, Text } from "react-native";
import { CustomInput } from "../../Components/CustomInput";
import instance from "../../Config/Axios";
import { Card, Container, Direction, Nav, NavText, CustomScrollView, Save, SaveText, CardTitle} from "./style";


class ArdorScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tssid: null,
      tpassword: null,
      trotate: null
    
    };
  }

  submitAnswer = () => {
    var ssid = this.state.tssid;
    var password = this.state.tpassword;
    var rotate = this.state.trotate;
    var send = "/setardor/" + String(ssid) + "/" + String(password) + "/" + String(rotate); 
    console.log(send);
    instance
      .get(send)
      .catch(function(error) {
        console.log(error);
      });
  };

  render() {
    return (
      <Container>
        <Direction>
          <Nav onPress={() => this.props.navigation.navigate('ZenScreen')}>
            <NavText>Zen</NavText>
          </Nav>
          <Nav onPress={() => this.props.navigation.navigate('ArdorScreen')}>
            <NavText>Ardor</NavText>
          </Nav>
        </Direction>

        <CustomScrollView showsVerticalScrollIndicator={false}>
          
          <Card alignItems={"flex-start"}>
            <CardTitle>WIFI</CardTitle>
            <CustomInput label="ssid" width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tssid: text
              })
            }></CustomInput>
            <CustomInput label="password" width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                tpassword: text
              })
            }></CustomInput>
          </Card>

          <Card alignItems={"flex-start"}>
            <CardTitle>Wheel rotations</CardTitle>
            <CustomInput width={"90%"} value={null}
              onChangeText={text =>
              this.setState({
                trotate: text
              })
            }></CustomInput>
          </Card>

        </CustomScrollView>

        <Save onPress={() => this.submitAnswer()}>
            <SaveText>Save</SaveText>
        </Save>

      </Container>
    );
  }
}

export default ArdorScreen;

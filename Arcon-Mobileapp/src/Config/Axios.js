import axios from "axios";
import { Platform } from "react-native";

const dev = "http://192.168.43.230:3000";

console.log("OS: ", Platform.OS, dev);

const instance = axios.create({
  baseURL: dev,
  timeout: 1000
});

export default instance;

import React from "react";
import { Text, TextInput, View } from "react-native";
import styled from "styled-components";

const Container = styled(View)`
  width: 100%;
`;

const Input = styled(TextInput)`
  border-radius: 4px;
  background-color: #f3f3f3;
  border: 0.5px solid #f7f7f7;
  font-size: 20px;
  marginLeft:15px;
  marginBottom:10px;
  color:#7e7e7e;
`;

const T = styled(Text)`
    marginLeft:15px;
    color:#7e7e7e;
`;

export const CustomInput = ({
  label,
  password,
  onChange,
  value,
  width,
  ...other
}) => (
  <Container>
    <T>{label}</T>
    <Input
      secureTextEntry={password}
      onChange={onChange}
      value={value}
      width={width}
      {...other}
    ></Input>
  </Container>
);
